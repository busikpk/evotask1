from pprint import pprint
from copy import deepcopy
import re


def update(data, service, count):
    ans = deepcopy(data)

    while count > 0:
        host_with_min_sum = ans[min(ans, key=lambda x: sum(ans[x].values()))]
        host_with_min_sum[service] = host_with_min_sum[service] + 1 if service in host_with_min_sum else 1
        count -= 1

    return ans


def main():
    example_data = {
        'ginger': {
            'django': 2,
            'flask': 3,
        },
        'cucumber': {
            'flask': 1,
        },
        'potato': {
            'aiohttp': 5
        }
    }

    print("Configuration before:")
    pprint(example_data)

    new_config = update(example_data, 'pylons', 7)
    new_config2 = update(new_config, 'pyramid', 6)

    print("Configuration after:")
    pprint(new_config2)


def main_interactive():
    print('Options:\n1)add new host enter: host HOST_NAME(str)\n'
          '2)add new service enter: service SERVICE_NAME(str) INSTANCES_COUNT(int)\n'
          '3)print current state enter: state'
          '4)exit q!')
    matches = [
        lambda x: re.findall('^\s*(service) \s*([A-Za-z0-9]+) \s*([0-9]+)\s*$', x),
        lambda x: re.findall('^\s*(host) \s*([A-Za-z0-9]+)\s*$', x),
        lambda x: re.findall('^\s*(state)\s*$', x),
        lambda x: re.findall('^\s*(q!)\s*$', x)
    ]
    config = {}
    while True:
        command = input('New Command:')
        for match_fn in matches:
            res = match_fn(command)
            if res:
                if res[0][0] == 'service':
                    if config.keys():
                        config = update(config, res[0][1], int(res[0][2]))
                    else:
                        print('first add host')
                elif res[0][0] == 'host':
                    host_name = res[0][1]
                    if host_name not in config:
                        config[host_name] = {}
                    else:
                        print('host "{}" already in state'.format(host_name))
                elif res[0] == 'state':
                    pprint(config)
                elif res[0] == 'q!':
                    print('goodbye!')
                    exit(0)
                break
        else:
            print('unknown command')

if __name__ == '__main__':
    #main()
    main_interactive()
